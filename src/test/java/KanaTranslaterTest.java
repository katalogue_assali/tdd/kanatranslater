import org.example.KanaTranslater;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class KanaTranslaterTest {


    @DisplayName("should translate syllable to kana")
    @ParameterizedTest(name = "{index} => should return {1} when translate {0}")
    @CsvSource({"a,あ", "ka,か", "i,い", "A,あ", "ni,に", "n,ん"})
    void should_return_the_kana_when_translate_syllable(String syllable, String kana) {
        assertThat(KanaTranslater.translate(syllable)).isEqualTo(kana);
    }

    @DisplayName("should translate syllables to hiragana")
    @ParameterizedTest(name = "{index} => should return {1} when translate {0}")
    @CsvSource({"aka,あか", "iki,いき", "uku,うく", "uk2u,うk2u"})
    void should_return_hiragana_when_translate_syllables(String syllables, String hiragana) {
        assertThat(KanaTranslater.translate(syllables)).isEqualTo(hiragana);
    }

    @Test
    void should_return_syllable_when_translate_unknown_syllable() {
        assertThat(KanaTranslater.translate("2")).isEqualTo("2");
    }

    @Test
    void translate_should_return_empty_result_if_empty_syllable_is_given() {
        assertThat(KanaTranslater.translate("")).isEqualTo("");
    }

    @Test
    void translate_should_return_empty_if_space_is_given() {
        assertThat(KanaTranslater.translate(" ")).isEqualTo("");
    }

    @DisplayName("should translate word to hiragana")
    @ParameterizedTest(name = "{index} => should translate {0} to {1}")
    @CsvSource({"Konnichiwa,こんにちわ", "Itatakimasu,いたたきます", "Arikato kosaimasu,ありかとこさいます", "nani !!!,なに!!!"})
    void should_translate_word_to_hiragana(String word, String hiragana) {
        assertThat(KanaTranslater.translate(word)).isEqualTo(hiragana);
    }
}
