import org.example.SyllableSplitter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SyllableSplitterTest {
    @DisplayName("should split multiple syllable")
    @ParameterizedTest(name = "{index} => should return [{1},{2}] when splitting {0}")
    @CsvSource({"aka,a,ka", "iki,i,ki", "uku,u,ku"})
    void should_split_word_in_multiple_syllable(String syllables, String expectedSyllable1, String expectedSyllable2) {
        assertThat(SyllableSplitter.split(syllables)).isEqualTo(List.of(expectedSyllable1, expectedSyllable2));
    }

}
