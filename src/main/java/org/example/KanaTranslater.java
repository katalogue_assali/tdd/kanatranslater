package org.example;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Map.entry;

public class KanaTranslater {
    static final String SYLLABLE_N = "n";
    static final Map<String, String> syllableToKana = Map.ofEntries(entry("a", "あ"), entry("i", "い"), entry("u", "う"), entry("e", "え"), entry("o", "お"), entry("ka", "か"), entry("ki", "き"), entry("ku", "く"), entry("ke", "け"), entry("ko", "こ"), entry("sa", "さ"), entry("shi", "し"), entry("su", "す"), entry("se", "せ"), entry("so", "そ"), entry("ta", "た"), entry("chi", "ち"), entry("tsu", "つ"), entry("te", "て"), entry("to", "と"), entry("na", "な"), entry("ni", "に"), entry("nu", "ぬ"), entry("ne", "ね"), entry("no", "の"), entry("ha", "は"), entry("hi", "ひ"), entry("fu", "ふ"), entry("he", "へ"), entry("ho", "ほ"), entry("ma", "ま"), entry("mi", "み"), entry("mu", "む"), entry("me", "め"), entry("mo", "も"), entry("yu", "ゆ"), entry("yo", "よ"), entry("ra", "ら"), entry("ri", "り"), entry("ru", "る"), entry("re", "れ"), entry("ro", "ろ"), entry("wa", "わ"), entry("wi", "ゐ"), entry("we", "ゑ"), entry("wo", "を"), entry("n", "ん"));

    static final char[] vowels = new char[]{'a', 'i', 'e', 'o', 'u'};

    public static String translate(String syllable) {
        String validSyllable = syllable.toLowerCase().replace(" ", "");
        return SyllableSplitter.split(validSyllable).stream()
                .map(s -> syllableToKana.getOrDefault(s, s))
                .reduce("", (partialString, element) -> partialString + element);
    }

    static List<String> getKeyToReversedOrderByLength() {
        return KanaTranslater.syllableToKana.keySet().stream()
                .sorted(Comparator.comparingInt(String::length).reversed())
                .collect(Collectors.toList());
    }
}
