package org.example;

public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.print("Konnichiwa translate to hiragana : ");
            System.out.println(KanaTranslater.translate("Konnichiwa"));
        } else if (args.length == 1) {
            System.out.print(args[0] + " translate to hiragana : ");
            System.out.println(KanaTranslater.translate(args[0]));
        } else {
            StringBuilder sentense = new StringBuilder();
            for (String words : args) {
                sentense.append(words);
                sentense.append(" ");
            }
            System.out.print(sentense + "translate to hiragana : ");
            System.out.println(KanaTranslater.translate(sentense.toString()));
        }
    }
}