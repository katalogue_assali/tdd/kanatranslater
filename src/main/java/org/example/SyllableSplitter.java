package org.example;

import java.util.ArrayList;
import java.util.List;

import static org.example.KanaTranslater.SYLLABLE_N;

public class SyllableSplitter {
    private static final int MAX_LENGTH_FOR_SYLLABLE_N = 2;

    public static List<String> split(String word) {
        List<String> syllables = new ArrayList<>();
        StringBuilder currentSyllable = new StringBuilder();
        for (int index = 0; index < word.length(); index++) {
            index = setIndex(word, currentSyllable, index);
            for (String syllable : KanaTranslater.getKeyToReversedOrderByLength()) {
                currentSyllable = appendSyllable(syllables, currentSyllable, syllable);
            }
        }
        if (!currentSyllable.toString().isEmpty()) syllables.add(currentSyllable.toString());
        return syllables;
    }

    private static StringBuilder appendSyllable(List<String> syllables, StringBuilder currentSyllable, String syllable) {
        if (currentSyllable.toString().equals(syllable)) {
            syllables.add(currentSyllable.toString());
            currentSyllable = new StringBuilder();
        }
        return currentSyllable;
    }

    private static int setIndex(String word, StringBuilder currentSyllable, int index) {
        try {
            if (String.valueOf(word.charAt(index)).equals(SYLLABLE_N)) {
                //StringIndexOutOfBoundsException can be throw if word finish by 'n'
                currentSyllable.append(particularCaseForSyllableN(word.charAt(index + 1)));
                if (currentSyllable.toString().length() == MAX_LENGTH_FOR_SYLLABLE_N) index++;
            } else currentSyllable.append(word.charAt(index));
        } catch (StringIndexOutOfBoundsException e) {
            //just append 'n'
            currentSyllable.append(SYLLABLE_N);
        }
        return index;
    }

    private static String particularCaseForSyllableN(char character) {
        for (Character vowel : KanaTranslater.vowels) {
            if (character == vowel) return SYLLABLE_N + character;
        }
        return SYLLABLE_N;
    }
}
